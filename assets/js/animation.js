
$(document).ready(function(){
  function init(){
    // Sidenav
    $('.sidenav').sidenav({
      draggable: true,
      preventScrolling: false
    })


    // Profil
    $(document).ready(function(){
      $('.collapsible').collapsible();
    });
    $('#fadeProfil').on('click', function(e){
      e.prevent.default();
      $('.profil_card').removeClass('card--show');
      $('.profil_card').addClass('card--hide');
       document.location.href="index.php?url=error"
  m
      return true;
    });

    $('.profil_card').removeClass('card--hide');
    $('.profil_card').addClass('card--show');


    // Typo
    $('.ml11 .letters').each(function(){
      $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
    });

    anime.timeline({loop: true})
      .add({
        targets: '.ml11 .line',
        scaleY: [0,1],
        opacity: [0.5,1],
        easing: "easeOutExpo",
        duration: 700
      })
      .add({
        targets: '.ml11 .line',
        translateX: [0,$(".ml11 .letters").width()],
        easing: "easeOutExpo",
        duration: 700,
        delay: 100
      }).add({
        targets: '.ml11 .letter',
        opacity: [0,1],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=775',
        delay: function(el, i) {
          return 34 * (i+1)
        }
      }).add({
        targets: '.ml11',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
      });

    $('.home_button').hide();
    setTimeout( function(){
      $('.home_button').show();
    }, 800)

  }
  // Swup
  const swup = new Swup();
  init();
  swup.on('contentReplaced', init);

});