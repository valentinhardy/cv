
<div class="container-fluid">
    <div class="error_page row">
        <div class="error_page-container  text-light col-lg-12">
            <h2 class="error_page-container--title text-center">Oups!</h2>
            <p  class="error_page-container--paragraph text-center">La page demandée n'existe pas.</p>
            <img src="https://media.giphy.com/media/FS7XJMuZUAkqA/giphy.gif" alt="Error 404">
        </div>
    </div>
</div>