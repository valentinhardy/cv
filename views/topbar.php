<nav>
    <div class="nav-wrapper">
      <div class="container">
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="large material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <?php if($_GET['url'] != 'home'){ ?>
          <li class="navbar_item"><a id="fadeProfil" href="/home">Accueil</a></li>
          <li class="navbar_item"><a href="/profil">Profil</a></li>
          <?php }?>
        </ul>
      </div>
    </div>
  </nav>
  <ul class="sidenav" id="mobile-demo">
    <li><a href="/home">Home</a></li>
    <li><a href="/profil">Profil</a></li>
  </ul>