<script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "Person",
    "name": "Hardy Valentin",
    "url": "http://hardy.valentin.com",
    "image": "http://monimage.fr",
    "sameAs": [
        "https://www.facebook.com/vhar1993",
        "www.linkedin.com/in/hardyvalentin"
    ],
    "jobTitle": "Web Developer"  
    }
</script>

<section id="home" class="white-text">
    <div class="col s12 l6 center-align home_item black white-text">
    <h1 class="ml11">
  <span class="text-wrapper">
    <span class="line line1"></span>
    <span class="letters">Valentin Hardy</span>
  </span>
</h1>
<a href="/profil" class="home_button waves-effect waves-light black white-outline btn-large">Acceder au profil</a>
            </div>
</section>
