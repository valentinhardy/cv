<footer class="page-footer">
    <div class="footer-copyright">
        <div class="footer_container container">
            <div class="copyright">
            © 2019 Valentin Hardy
            </div>
            <div class="img_footer">
            <img src="./assets/img/linkedin.png" alt="linkedin">
            <img src="./assets/img/gitlab.png" alt="gitlab">
            </div>
        </div>
    </div>
</footer>