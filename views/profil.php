<section id="profil" class="valign-wrapper">
    <div class="row">
        <div class="profil_card col s12 l6 black-text card--hide">
            <div class="card medium ">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="./assets/img/post.jpg">

                </div>
                <div class="card-content ">
                    <h2 class="card-title  green-text lighten-2 ">Compétences</h2>
                    <a class="btn-floating activator halfway-fab waves-effect waves-light green"><i class="material-icons">add</i></a>
                    </h2>
                </div>
                <div class="card-reveal white-text ">
                    <span class="card-title green-text lighten-2">Compétences<i
                            class="material-icons right">close</i></span>
                    
                        <h6 class="green-text lighten-2">Build / Refonte graphique</h6>
                        <p>- Intégrer des pages web & landing pages sur la base d'une maquette (utilisation de Sass, Grunt,
                        Gulp, Webpack...)
                        - Réalisation de mails responsive a l'aide du framework MJML
                        - Installation de modules (Prestashop/Magento/Wordpress)
                        - Utilisation de Bootstrap, Foundation, Materialize..</p>
                        <h6 class="green-text lighten-2">TMA / Tiers Maintenance Applicative</h6>
                        <p>- Maintenance & Correctifs Front sur des sites eCommerce
                        - Evolutions Front commandées par le client en relation avec le Chef de Projet TMA</p>  
                    
                </div>
            </div>
        </div>
        <div class="profil_card col s12 l6 black-text card--hide">
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="./assets/img/post.jpg">

                </div>
                <div class="card-content">
                <h2 class="card-title blue-text">Formation</h2>
                <a class="btn-floating activator halfway-fab waves-effect waves-light blue "><i class="material-icons">add</i></a>
                    </h2>
                </div>
                <div class="card-reveal white-text">
                    <span class="card-title blue-text lighten-2">Formation<i
                            class="material-icons right">close</i></span>
                    <h6 class="blue-text lighten-2">Juin 2018 / Aout 2018 - Open Declic - OpenClassrooms</h6>
                        <p>- Réaliser un site OnePage
                        - Découvrir Wordpress, customiser un thème enfant</p>
                        <h6 class="blue-text lighten-2">Aout2018 / Aout2019 - DUT Développeur Web Junior - OpenClassrooms</h6>
                        <p>- Réaliser une application de réservation de Vélo
                        - Créer un moteur de blog MVC en PHP
                        - Projet personnel utilisant HTML/CSS/JS/PHP</p>
                        <h6 class="blue-text lighten-2">Aout2018 / Aout2019 - Développeur Web Junior - Insitaction</h6>
                        <p>Alternance dans le cadre du DUT Developpeur Web Junior</p>

                    </p>
                </div>
            </div>
        </div>
    </div>
</section>