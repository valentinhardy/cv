<section id="dev">
        <div class="row">
            <div class="col-sm-12 col-lg-12 dev_list">
                <ul class="about_container--list">
                    <h3>Dev. Front</h3>
                    <li class="about_container--list_item">
                        <img src="./assets/img/html.png" alt="html" />
                        <img src="./assets/img/css.png" alt="css" />
                        <img src="./assets/img/js.png" alt="js" />
                        <img src="./assets/img/react.png" alt="react" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-12 dev_list">
                <ul class="about_container--list">
                    <h3>Dev. Back</h3>
                    <li class="about_container--list_item">
                        <img src="./assets/img/php.png" alt="php" />
                        <img src="./assets/img/prestashop.png" alt="prestashop" />
                        <img src="./assets/img/wordpress.png" alt="wp" />
                    </li>
                </ul>
            </div>
        </div>
    </section>
    
    
<!-- Outils & SEO -->
<section id="other">
    <div class="row">
        <div class="col-sm-12 col-lg-12 dev_list">
            <ul class="about_container--list">
                <h3>Outils</h3>
                <li class="about_container--list_item">
                    <div> Gitlab<img src="./assets/img/gitlab.png" alt="gitlab" />
                    </div>
                    <div><img src="./assets/img/photoshop.png" alt="photoshop" /> Photoshop</div>
                    <div>MJML<a target="_blank" href="http://mjml.io"><img src="./assets/img/mjml.png" alt="mjml" /></a></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12 dev_list">
            <ul class="about_container--list">
                <h3>Search Engine<br>Optimization</h3>
                <li class="about_container--list_item">
                    - Données structurées
                </li>
                <li class="about_container--list_item">
                    - Balisage Sémantique
                </li>
                <li class="about_container--list_item">
                    - Google Tag Manager
                </li>
            </ul>
        </div>
    </div>
</section>